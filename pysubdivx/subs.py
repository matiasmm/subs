#!/usr/bin/env python3

from pyquery import PyQuery as pq
from lxml import etree
import urllib
import os
from os.path import basename
import guessit
from urllib.request import urlopen
from urllib import parse as urlparse
from urllib.parse import urlsplit
import zipfile, rarfile
import shutil


def get_subs_for_movie(title, path_movie, data):
    if os.path.exists(path_movie[:-4] + '.srt'):
        return
    print("#### {} ####".format(title))
    url_subs = 'http://www.subdivx.com/index.php?buscar={}&accion=5&masdesc=&subtitulos=1&realiza_b=1'.format(title)

    d = pq(url=url_subs)

    details = d('#buscador_detalle')
    default = None 
    search_word = None 

    results, default, k = print_list(d, details, data['releaseGroup'].lower() if 'releaseGroup' in data else None)

    choice = None
    while type(choice) != int or choice < 1 or choice > k+1:
        prompt = "Now is your choice. Suggestion '{}': ".format(default+1) if default is not None else "Now is your choice: "
        search_word = input(prompt).strip().lower()
        if search_word.isdigit():
            choice = int(search_word)
        elif search_word != "":
            results, default, k = print_list(d, details, search_word)

    chosen = results[choice-1]
    r = urlopen(chosen['url'])

    localName = url2name(chosen['url'])
    if 'Content-Disposition' in r.info():
        # If the response has Content-Disposition, we take file name from it
        localName = r.info()['Content-Disposition'].split('filename=')[1]
        if localName[0] == '"' or localName[0] == "'":
            localName = localName[1:-1]
    elif r.url != chosen['url']: 
        # if we were redirected, the real file name we take from the final URL
        localName = url2name(r.url)

    # Save downloaded file to .subs directory
    f = open('.subs/' + localName, 'wb')
    f.write(r.read())
    f.close()
    downloaded = '.subs/' + localName

    if '.zip' == localName[-4:]:
        zf = zipfile.ZipFile(downloaded)
        for zname in zf.namelist():
            if zname[-4:] == '.srt':
                source = zf.open(zname)
                target = open(path_movie[:-4] + zname[-4:], 'wb')
                with source, target:
                    shutil.copyfileobj(source, target)
    elif '.rar' == localName[-4:]:
        rf = rarfile.RarFile(downloaded)
        for rname in rf.namelist():
            if rname[-4:] == '.srt':
                source = rf.open(rname)
                target = open(path_movie[:-4] + rname[-4:], 'wb')
                with source, target:
                    shutil.copyfileobj(source, target)
    else:
        print('unkown format '+localName[-4:])
        exit()

def print_list(d, details, search_word):
    results = []
    default = None
    k = None
    for k, detail in enumerate(details):
        title = d(detail).prev().find('.titulo_menu_izq').html();
        dsc = d(detail).find('#buscador_detalle_sub').html()
        url = d(detail).find('a[rel=nofollow][target=new]').attr('href')
        if search_word is not None and search_word in dsc.lower():
            if default is None:
                default = k
            print("\n\033[32m{}-{}\033[0m \n\t{}".format(k+1, title, dsc))
        else:
            print("\n{}-{} \n\t{}".format(k+1, title, dsc))

        results.append({'dsc': dsc, 'url':url})

    return results, default, k

def url2name(url):
    return basename(urlsplit(url)[2])



try:
    if not os.path.exists('.subs'):
        os.makedirs('.subs')

    for f in os.listdir('.'):
        if f[0] != '.' and f[-4:].lower() in ('.avi', '.mp4', '.mpg', '.mkv'):
            guess = guessit.guess_movie_info(f)
            get_subs_for_movie(guess['title'], f, guess)
except KeyboardInterrupt:
    print(" Bye!")

